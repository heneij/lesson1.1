﻿using System;

namespace Lesson_1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number: ");
            bool suc = int.TryParse(Console.ReadLine(), out int num);
            if (suc && num > 0)
            {
                Console.WriteLine("You entered " + num.ToString() + ". Good job!");
            }
            else
            {
                Console.WriteLine("You entered an invalid number, please try again!");
                return;
            }
            Console.WriteLine("Here is a " + num.ToString() + " by " + num.ToString() + " square:\n");

            Console.WriteLine(new string('*', num));
            for (int i = 0; i < num - 2; i++)
            {
                if (i == 0 || i == num - 3 || num < 4)
                {
                    Console.WriteLine('*' + new string(' ', num - 2) + '*');
                }
                else if(i == 1 || i == num - 4)
                {
                    Console.WriteLine("* " + new string('*', num - 4) + " *");
                }
                else if(num == 5)
                {
                    Console.WriteLine("* * *");
                }
                else
                {
                    Console.WriteLine("* *" + new string(' ', num - 6) + "* *");
                }

            }
            Console.WriteLine(new string('*', num));

        }
    }
}
